<?php

/* 
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Scripting/EmptyPHP.php to edit this template
 */
?>
<html>
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">

        <title><?=$title?></title>
    </head>
    <body>
         <h1 class="text-primary"><?= $title?></h1>
        <form action="<?= site_url('')?>" method="post">
        <table class="table table-striped">
            <?php foreach ($grupos as $grupo): ?>
                <tr>
                    <td>
                        <?= $grupo->id ?>
                    </td>
                    <td>
                        <a href="http://localhost/codeigniter/alumno/lista/<?= $grupo->codigo ?>"><?= $grupo->codigo ?> </a>
                    </td>
                    <td>
                        <?= $grupo->nombre ?>
                    </td>
                </tr>

            <?php endforeach; ?>
        </table>
        </form>
    </body>
</html>
